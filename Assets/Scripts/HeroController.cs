﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour {

    public float Acceleration = 1;
    public float jumpForce = 500f;
    Vector3 Dir = new Vector3(0, 0, 0);
    private bool jump = false;

	private void Update()
	{
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
        }
	}

	void FixedUpdate()
	{
        if (Input.GetKey(KeyCode.A))
        {
            Dir.x = -Acceleration;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            Dir.x = Acceleration;
        }
        else
        {
            Dir.x = 0;
        }
        transform.position += Dir * Time.deltaTime;
        if (jump)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }
	}
}
