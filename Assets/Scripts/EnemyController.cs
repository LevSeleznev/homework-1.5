﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ENEMY_STATE
{
    PATROL = 1,
    ATTACK = 2
};

public class EnemyController : MonoBehaviour {

    public ENEMY_STATE CurrentState = ENEMY_STATE.PATROL;
    private bool CanSeePlayer;

    public IEnumerator StatePatrol ()
    {
        CurrentState = ENEMY_STATE.PATROL;
        while (CurrentState == ENEMY_STATE.PATROL)
        {
            if (CanSeePlayer)
            {
                StartCoroutine(StateAttack());
                yield break;
            }
            yield return null;
        }            
    }

    public IEnumerator StateAttack ()
    {
        CurrentState = ENEMY_STATE.ATTACK;
        while (CurrentState == ENEMY_STATE.ATTACK)
        {
            if (!CanSeePlayer)
            {
                StartCoroutine(StatePatrol());
                yield break;
            }
            yield return null;
        }
    }
}
